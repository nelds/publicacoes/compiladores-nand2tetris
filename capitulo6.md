# Capítulo 6 - Linguagem intermediária e ambiente de execução

Esse ambiente é baseado em uma máquina de pilha, ou seja, todos os comandos utilizam uma pilha como "área de trabalho".

![pictures/cap6/Untitled.png](pictures/cap6/Untitled.png)

Em um ambiente de execução precisamos considerar a organização da memória. Nessa máquina virtual, cada célula da memória armazena 16 bits que são utilizados para representar todos os valores inteiros, booleanos e endereço de memória.

- Um valor inteiro em complemento de 2, variando entre  -32768 e 32767.
- Os valores booleanos usam 0 para falso e -1 para verdadeiro. Dado que -1, seria representado com todos os bits em 1.
- Um endereço de memória também representado por um valor inteiro.

A memória é dividida em segmentos, que serão utilizados para armazenar os diferentes tipos de variáveis. As variáveis static, field, local e argument são mapeadas pelo compilador nos quatro segmentos de memória static, this, local, argument. Cada variável terá uma posição dentro destes segmentos, por exemplo, uma variável x poderá ser mapeada para o local 1. Além disso, existem mais quatro segmentos de memória adicionais, cuja função será apresentada posteriormente: that, constant, pointer, temp. O emulador da máquina virtual, possibilita visualizar os valores dentro de cada segmento:

![pictures/cap6/Untitled%201.png](pictures/cap6/Untitled%201.png)

Além dos oito segmentos, destaca-se as seguintes áreas:

- **Stack**: A pilha é usada por esse tipo de máquina para armazenar os valores temporários..
- **Heap**: O heap é o nome da área de RAM dedicada ao armazenamento de objetos e dados de matrizes. Esses objetos e matrizes podem ser manipulados por comandos VM, como veremos em breve.

### Acesso e operações sobre os dados

Os comandos de acesso a memória são apenas dois, o push e pop. O primeiro empilha um elemento de um dado segmento de memória, e o segundo desempilha um elemento e copia para uma dada posição da memória.

![pictures/cap6/Untitled%202.png](pictures/cap6/Untitled%202.png)

Considerando os segmentos, é possível realizar as seguintes operações:

**Empilhar**

A sintaxe básica para empilhar um elemento na pilha é:

```java
push segment index
```

Essa operação empilha o valor do segmento [índice] na pilha. Onde “segmento” pode ser argument, local, static, constant, this, that, pointer ou temp. O índice é um número inteiro não negativo.

**Desempilhar**

A sintaxe básica para desempilha um elemento da pilha é:

```java
pop segment index
```

Desempilha o valor do topo da pilha e armazena-o no segmento [índice]. Onde “segment” pode ser argument, local, static, this, that, pointer ou temp. O índice é um número inteiro não negativo.

**Operações lógicas e aritméticas**

A linguagem intermediária suporta nove comandos aritméticos e binários. Destes, sete são comandos binários, ou seja, eles desempilham dois elementos da pilha, computa o comando e armazena o resultado novamente na pilha. 

Apenas dois comandos são unários, especificamente os comandos neg e not, neste caso eles são aplicados a apenas um valor da pilha. Três comandos tem como retorno valores lógicos. 

Na linguagem intermediária os valores lógicos verdadeiro e falso são representados como -1 (0xFFFF) e 0 (0x0000), respectivamente

- add
- sub
- neg
- eq	if x = t then true else false
- gt	if x > t then true else false
- lt	if x < t then true else false
- and
- or
- not

### Controle de fluxo

Alguns casos, não desejamos que todos os comandos sejam executados, ou que partes do código seja executado uma ou mais vezes.  Em linguagens de mais alto nível, quando queremos executar apenas alguns comandos, usamos comandos condicionais, como a estrutura “if-else”, por exemplo:

```java
a = 20;
b = 30;
if (a > b)
  maior = a;
else
  maior = b;
```

Usamos comandos como `while` ou `for` para executar trechos de códigos mais uma vez:

```java
i = 1;
soma = 0;
while (i < 10) {
	i = i + 1;
	soma = soma + i;
}
```

Na linguagem intermediária, usamos saltos condicionais e incondicionais. Assim, será mais fácil a tradução para uma linguagem de baixo nível que já possui esse tipo de estrutura.

O controle de fluxo é dado por três comandos:

- **label symbo**l: declara um label, para aonde uma instrução goto, ou if-goto pode levar
- **goto symbol**: salto incondicional para uma dada posição
- **if-goto symbol**: salto condicional, se o valor na pilha for verdadeiro, salta para uma dada posição.

Para ilustrar, considerem um programa que empilha dois valores, caso eles seja iguais, imprime 32, se diferentes imprime 64. Por que esses valores ? Por que sim 🙂

```cpp
function Main.main 0

push constant 10
push constant 10
eq

if-goto iguais
push constant 64
call Output.printInt 1
goto fim

label iguais

push constant 32
call Output.printInt 1
label fim

return
```

### Procedimentos

Um programa na linguagem intermediaria é uma coleção de funções ou procedimento, ou seja, poderíamos considerá-la como uma linguagem procedimental. Procedimentos, funções, métodos e construtores serão todos mapeados para funções. 

 Considere a execução da simples expressão 10+15.

```wasm
function Main.main 0
push constant 15
push constant 10
add
call Output.printInt 1
return
```

Importante destacar, que são comando e não definições de funções. Temos apenas  três comandos relacionados a funções.

Criação de função

Cria-se uma função definindo o seu nome, e a quantidade de variáveis locais que ela possui. Por exemplo, abaixo esta declarado uma função chamada dobro com 0 variáveis locais.

```
function dobro 0
```

Então, para o ambiente de execução esse comando cria todo o contexto para a execução dessa função. Os comandos que se seguem usaram esse contexto criado. Por exemplo, o seguinte programa irá gerar um erro, dado que não existe uma nenhuma variável local:

```java
function Main.main 0
push constant 10
pop local 0
push constant 0
return
```

Chamada de função

Chamada de função: dado uma função declarada previamente, pode-se então chamar sua execução. Como todas funções são executadas também na pilha é necessária indicar o nome da função e a quantidade de argumentos que ela irá consumir na pilha. Por exemplo, para calcular a seguinte expressão `sqrt(x-17+x*5)`, usaríamos o seguinte código:

```cpp
push constant 2 // valor de x
pop local 0
push local  0
push constant 17
sub
push local 0
push constant 5
call Math.multiply 2
add
call Math.sqrt 1
```

Observe que esse ambiente de execução abstrai todo o processo de mudança de contexto. Quando voltar terá o acesso a função em execução terá todo os seus segmentos com os mesmos valores de antes da execução.

Retorno

Ao termino de um cálculo ou de um algoritmo, o resultado será retornado pelo comando return. Esse comando consome um valor da pilha, e repassa o controle de execução para a função chamadora.

```cpp
return
```

Observe que será consumido um valor da pilha. Então, mesmo procedimentos que não retornam valor, será necessário empilhar um valor na pilha. Nesse caso a abordagem comum é empilhar um valor nulo. Essa abordagem pode ser observada em outras linguagens, por exemplo:

```python
def inutil():
  pass

print (inutil())
```

A execução do programa em Python acima irá imprimir `None`.

## **Exemplo de um programa simples**

Para ilustrar o seguinte comando, podemos escrever um código na linguagem intermediária equivalente ao seguinte código em C:

```cpp
     int dobro (int x) {
        return 2 * x;
    }
    
    int main () {
        int a = 12;
        int b = dobro (a);
				printf ("%d\n",b);
        return 0;
    }
```

ou em Python

```rust
import dis

def dobro (x):
  return 2 * x

def main () :
  a = 12
  b = dobro(a)
  print(b)
        
dis.dis(main)
```

Para rodarmos o código no emulador de máquina virtual precisamos seguir alguns protocolos. Todo programa na linguagem intermediária, inicia pela execução de uma função chamada Main.main, que é então a função principal. Consideraremos aqui Main, como o nome de um modulo que poderá possuir varias funções. Essas funções serão salvas em um arquivo “Main.vm”, com a seguinte codificação:

```cpp
function Main.dobro 0
push constant 2
push argument 0
call Math.multiply 2
return

function Main.main 2
push constant 12
pop local 0
push local 0
call Main.dobro 1
pop local 1
push local 1
call Output.printInt 1
push constant 0
return
```

<aside>
📢 A endentação é opcional, usado aqui apenas para melhorar a leitura do código.

</aside>

Agora vamos ver um programa para imprimir de 1 a 10 em pseudocodigo:

```rust
i = 1
label teste
if i > 9 goto fim
i = i + 1
print (i)
goto teste
label fim
```

O pseudocódigo acima, poderia ser mapeado para a linguagem intermediária com abaixo:

```rust
function Main.main 1
push constant 1
pop local 0  // i = 1
label teste
push local 0
push constant 9
gt
if-goto fim // if i > 9 goto fim

push local 0        
call Output.printInt 1  // print (i)

call Output.println 0 // prinln

push local 0
push constant 1
add
pop local 0 // i = i + 1
goto teste

label fim
push constant 0
return
```
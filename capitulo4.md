# Capítulo 4: A linguagem Jack

## 1 Introdução

Neste estudo, será utilizado a linguagem de programação denominada Jack, desenvolvida no contexto do projeto Nand2Tetris. Essa é uma linguagem que foi desenvolvida como o objetivo didático, inclusive tendo vários elementos que facilitam o desenvolvimento de um compilador. Pode-se destacar as seguintes características:

- Sintaxe inspirada no Java
- Baseada em classe, mas sem recursos como herança e polimorfismo
- Uso geral
- Aprendizado fácil
- Não existe precedência de operadores
- Variáveis de instância são sempre privadas
- Métodos são sempre públicos
- Fracamente tipificada
- Não possui um tipo de dados equivalente ao `float`

Baixem [aqui](https://drive.google.com/file/u/1/d/1KcFPj8KQ_QAHheFmLCqs5iqC_0NCndvs/view?usp=sharing) as ferramentas disponibilizadas pelo projeto nand2tetris.Documentação das ferramentas, estão disponíveis no seguinte [site](https://www.nand2tetris.org/software).

A unidade básica de um programa Jack é a classe. Estas são definida em um arquivo único e compilada separadamente.

Um programa Jack é uma coleção de uma ou mais classes, e uma classe é uma coleção de uma ou mais subrotinas. Uma classe em Jack tem a seguinte estrutura:

```java
    class ClassName {
        variáveis de classe
        variáveis de instância
        
        construtor
        funções (método de classe) 
        métódos (método de instância)
        
    }

```

Sendo que para ser executado, é necessário que uma das classes seja nomeada como “Main”, e nela possua uma função nomeada de “main”, como no programa mínimo abaixo:

```java
class Main { 
     function void main () {
       var String nome;
       let nome = Keyboard.readLine("Qual o seu nome? ");
       do Output.printString("Ola "); 
       *do Output.printString(nome);* 
       do Output.println(); 
       return;
      }
}
```

Ferramentas necessárias para testar é o compilador e a maquina virtual.

Compilador, que dado um arquivo na linguagem Jack formato `jack`, converte para o formato intermediário, que poderá ser executado por um emulador de maquina virtual

VM Emulator, é a máquina virtual, depois de compilado, basta abrir e executar o código no formato `.vm`

---

## 2 Tipos e valores

A linguagem Jack é uma linguagem estaticamente tipificada, ou seja, toda variável tem que ser declarada previamente e possuir um tipo de dado que não modifica durante a execução.

A linguagem suporta apenas três tipos de dados **primitivos**:

| Tipo | Valores |
| --- | --- |
| int | 2 bytes (-32768 até 32767)) |
| boolean | true e false |
| char | unicode (‘a’, ‘x’, ‘+’, ‘%’, ...) |

Os tipos de dados compostos são criados a partir da especificação de novas classes.

A linguagem Jack suporta o uso de cadeia de caracteres (string) e coleções (arrays) através da sua biblioteca padrão:

- **String**, representando uma cadeia de caracteres demarcada por aspas duplas. Por exemplo: “Ola mundo”
- **Array**, representando uma estrutura de dados contígua e homogênea.

Na Seção “Biblioteca Padrão” apresenta alguns exemplos da utilização destes tipos de dados. Além destes, toda classe é tratada como um tipo de dado. Ou [click aqui](https://github.com/profsergiocosta/teaching-cp20201/blob/master/aulas/jack_api.pdf) para visualizar em detalhes a biblioteca padrão.

---

## 3 Variáveis

Como em outras linguagens estáticas, em Jack devemos declarar uma variável e associá-la a um tipo de dado. Além disso, no momento da declaração dela devemos especificar o seu escopo. Por exemplo, no codigo abaixo estamos declarando duas variáveis de instância do tipo int:

```java
field int x, y;
```

De modo análogo ao Java,  as variáveis podem ser declaradas no Escopo de classes ou no Escopo de subrotinas

No escopo de classe as variáveis podem ser:

- **Variáveis de classe**, similar ao Java, são definidas através da palavra reservada `static`. Elas são são *únicas por classe*, sendo compartilhadas por todas instâncias.
- **Variáveis de instância** são *únicas por objetos*, e diferentemente de Java, aqui elas são explicitamente declaradas através a palavra reservada `field.`

No exemplo a seguir …

```java
    class Point { 
        field int x, y; 
        static int pointCount; 
        ...
    }
```

<aside>
💡 As variáveis de instância são sempre privadas, e precisam de métodos gets e sets para poderem se acessadas e atualizadas

</aside>

Escopo de subrotinas (métodos, funções e construtores) as variáveis podem aparecer como parâmetros formais e variáveis locais.

Os parâmetros são declarados modo similar a linguagens como C, C++ e Java:

```java
method void setX(int x)
```

Com relação a declaração de variáveis locais, existe algumas diferenças e limitações comparado com linguagens de programação mais modernas. 

Primeiramente, as variáveis locais são declaradas com o uso explicito da palavra reservada `var`:

```java
function void main() { 
    var int n; 
    var int i, sum; 
    let length = Keyboard.readInt(”Entre com um inteiro:”); 
    ...
```

Além disso, as declarações sempre ocorrem no início da subrotina, e não é possível atribuir valores no momento da declaração. Por exemplo:

```java
function void main() { 
    var int sum = 0; // erro sintático
    ...
```

```java
function void main() { 
    var int sum; 
    let sum = 0;
    var int i; // erro sintático
    ...
```

---

## 4 Expressões

As expressões são formadas por **literais**, **variáveis**, **operadores** e **chamada de funções**. A linguagem Jack suporta as seguintes literais (ou constantes).

- "Ola", cadeias de caracteres
- 1256, inteiro sem sinal
- true e false, valores lógicos
- null, que significa uma referência não definida

<aside>
💡 Observe que devido a simplificações, a linguagem não suporta ponto flutuante.

</aside>

Como nas demais linguagens, **variáveis** declaradas previamente, podem ser usadas em expressões:

```java
base * altura
```

A linguagem suporta os quatro operadores aritméticos, operadores lógicos e relacionais. Como é uma linguagem cujo objetivo é facilitar o desenvolvimento do compilador, todos operadores são formados por apenas um caracter. 

- Operadores matemáticos binários: `+`, `-`, `*`, `/`
- Operador matemático unário `-`
- Operadores lógicos binário: `&` (e) , `|`(ou)
- Operadores lógico unário: `~` (não)
- Operadores relacionais: `<` , `>` e `=`

<aside>
📢 Observe, como é usado apenas um símbolo a linguagem não suporta os operadores `>=` e o `<=`

</aside>

<aside>
💡 Lembrando que não existe precedência de operadores em Jack

</aside>

Além de operadores, uma expressão pode ser composta também por uma ou mais chamadas de funções e ou métodos. Por exemplo, uma expressão válida:

```java
4 + 5 * Math.sqrt (16)
```

---

## 5 Comandos

A linguagem suporta comandos de atribuição, chamadas de procedimentos, seleção e iteração.

A atribuição é o comando mais comum em qualquer linguagem imperativa. Aqui toda atribuição inicia com a palavra reservada `let`:

```java
let a = 10;
```

Procedimentos podem ser entendidos como uma coleção de comandos que executa uma computação parametrizável. Na linguagem Jack os procedimentos são distintos das funções, dado que os procedimentos não fazem parte de expressões. São aqueles métodos ou função que foram declarados com `void` ao invés de um tipo.

Um procedimento é executado usando a palavra reservada `do`

```java
do Output.printString(”The average is ”);
```

A única estrutura de seleção existente é o if. Diferentemente de outras linguagens, os comandos dentro do if e else, sempre devem estar entre a abertura e o fechamento de chaves:

```java
class Main {
    function int maior (int a, int b) {
        if (a > b) {
            return a;
        } else {
            return b;
        }
    }

    function void main() {
        do Output.printInt( Main.maior (10,20));
        return;
    }
}
```

A única estrutura de iteração existente é o `while`:

```java
    function int fatorial (int n) {
        var int p;
        var int i;
        let p = 1;
        let i = 1;
        let n = n + 1;
        while (i < n) {
            let p = p * i;
            let i = i + 1;
        }

        return p;
    }
```

## 6 Construtores, métodos e funções

Além dos atributos (fields) e variáveis estáticas, as classes possuem construtores, métodos e funções.

Um construtor na linguagem Jack é definido a partir da palavra reservada `constructor` seguido pelo nome da classe e identificado como `new`a Novamente por simplificação, não existe sobrecarga de construtores,  ou seja, cada classe possui apenas um construtor:

```java
class Point { 
    field int x, y; 
    static int pointCount; 
    /** definição do construtor */ 
    constructor Point new(int ax, int ay) { 
        let x = ax; let y = ay; 
        let pointCount = pointCount + 1; 
        return this; 
    } 
    ... 
}
```

A chamada a um construtor inicia pelo nome da classe, seguido do identificador `new`.

```java
let pt = Point.new(10, 20); 
```

Os métodos de instância são definidos através da palavra reservada `method` e são sempre públicos. Todo método tem então a seguinte estrutura:

```java
method tipo (ou void) nomeDoMetodo ( lista de parâmetros e tipos) {
      variáveis locais
      comandos
}
```

Por exemplo:

```java
method int getArea() {
    return base*altura;
}
```

As funções podem ser comparadas aos métodos de classe na linguagem Java, porém é definida através da palavra reservada function. 

Toda função tem então a seguinte estrutura:

```java
function tipo (ou void) nomeDaFuncao ( lista de parâmetros e tipos) {
      variáveis locais
      comandos
}
```

Por exemplo, a função main():

```java
class Main {
    function void main() {
        var Array a;
        var int length;
        var int i, sum;
        let length = Keyboard.readInt(”How many numbers? ”);
        let a = Array.new(length); // constructs the array
        let i = 0;
        while (i < length) {
            let a[i] = Keyboard.readInt(”Enter a number: ”);
            let sum = sum + a[i];
            let i = i + 1;
        }
        do Output.printString(”The average is ”);
        do Output.printInt(sum / length);
        return;
    }
}
```

 

## 7 Biblioteca padrão

A biblioteca padrão possui funções para: Operações matemáticas, Cadeia de caractere, Array,  Operações de saída de texto, Operações de saída gráfica,  Operações de entrada de dados por teclado e Operações de gerência de memória. [Acesse aqui](https://github.com/profsergiocosta/teaching-cp20201/blob/master/aulas/jack_api.pdf) a descrição destas operações.

## 8 Descrição da sintaxe

### Notação

Antes de entrarmos em mais detalhes sobre a implementação do parser, vamos visualizar um subset da gramática. Para descrever a gramática, não foi usada a BNF original, e sim as seguintec conveções:

- ‘**xxx**’ : os simbolos terminais são apresentado em negrito e entre aspas simples.
- xxx: os simbolos não terminais usa fontes regulares.
- (): parenteses são usados para agrupar alguns construtores, usualmente dentro de um grupo de opções.
- x|y: alternativas, indica que pode ser usado `x` ou `y` indicates that either x or y can appear;
- x?: opcional até 1, indica que pode aparecer 0 ou 1 vez.
- x*: opcional vários, indica que pode aparecer 0 ou mais vezes.

### Elementos léxicos

| Termina         	| Regra                                                                           	|
|-----------------	|---------------------------------------------------------------------------------	|
| statements      	| statement*                                                                      	|
| statement       	| letStatement \| ifStatement \| whileStatement \| doStatement \| returnStatement 	|
| letStatement    	| 'let' varName ( '[' expression ']' )? '=' expression ';'                        	|
| ifStatement     	| 'if' '(' expression ')' '{' statements '}' ( 'else' '{' statements '}' )?       	|
| whileStatement  	| 'while' '(' expression ')' '{' statements '}'                                   	|
| doStatement     	| 'do' subroutineCall ';'                                                         	|
| ReturnStatement 	| 'return' expression? ';'                                                        	|

The Jack language includes five types of terminal elements (tokens):

| Terminal | Regra |
| --- | --- |
| keyword | 'class' | 'constructor' | 'function' |'method' | 'field' | 'static' | 'var' |'int' | 'char' | 'boolean' | 'void' |'true' |'false' | 'null' | 'this' | 'let' | 'do' |'if' | 'else' | 'while' | 'return' |
| symbol | '{' | '}' | '(' | ')' | '[' | ']' | '.' |',' | ';' | '+' | '-' | '' | '*' | '&' | '|' | '<' | '>' | '=' | '~' |
| integerConstant | A decimal number. |
| StringConstant | A sequence of Unicode characters not including double quote or newline. |
| identifier | A sequence of letters, digits, and underscore ( '_' ) not starting with a digit. |

### Elementos sintáticos

A Jack program is a collection of classes, each appearing in a separate file. The compilation unit is a class. A class is a sequence of tokens structured according to the following context free syntax:

| Não terminal | Regra |
| --- | --- |
| classdef | 'class' className '{' classVarDec* subroutineDec* '}' |
| classVarDec | ( 'static' | 'field' ) type varName ( ',' varName)* ';' |
| type | 'int' | 'char' | 'boolean' | className |
| subroutineDec | ( 'constructor' | 'function' | 'method' ) ( 'void' | type) subroutineName '(' parameterList ')' subroutineBody |
| className | identifier |
| parameterList | ((type varName) ( ',' type varName)*)? |
| subroutineBody | '{' varDec* statements '}' |
| varDec | 'var' type varName ( ',' varName)* ';' |
| subroutineName | identifier |
| varName | identifier |

**Statements**

| Não terminal | Regra |
| --- | --- |
| statements | statement* |
| statement | letStatement | ifStatement | whileStatement | doStatement | returnStatement |
| letStatement | 'let' varName ( '[' expression ']' )? '=' expression ';' |
| ifStatement | 'if' '(' expression ')' '{' statements '}' ( 'else' '{' statements '}' )? |
| whileStatement | 'while' '(' expression ')' '{' statements '}' |
| doStatement | 'do' subroutineCall ';' |
| ReturnStatement | 'return' expression? ';' |

**Expressions**

| Não terminal | Regra |
| --- | --- |
| expression | term (op term)* |
| term | integerConstant | stringConstant | keywordConstant | varName | varName '[' expression ']' | subroutineCall | '(' expression ')' | unaryOp term |
| subroutineCall | subroutineName '(' expressionList ')' | (className|varName) '.' subroutineName '(' expressionList ')' |
| expressionList | (expression ( ',' expression)* )? |
| op | '+' | '-' | '* | '/' | '&' | '|' | '<' | '>' | '=' |
| unaryOp | '-' | '~' |
| KeywordConstant | 'true | 'false' | 'null' | 'this' |

---

- Atividade
    - Mapeiem o seguinte programa em Java, para Jack: [https://repl.it/@SergioSouza1/bbox-java#Main.java](https://repl.it/@SergioSouza1/bbox-java#Main.java)
- Saber mais
    
    Nos slides e vídeos abaixo, vocês poderão acessar excelentes materiais que foram a base desse conteúdo.
    
    - [Slides - nand2tetris](https://drive.google.com/file/d/1rbHGZV8AK4UalmdJyivgt0fpPiD1Q6Vk/view)
    - [Vídeos aulas - nand2tetris](https://www.coursera.org/learn/nand2tetris2/home/week/3)
    - [Exemplo de código que escrevi para "brincar" com a linguagem jack](https://github.com/profsergiocosta/MemoryGame)